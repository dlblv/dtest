# DTest
## How to run?
### Client
`cargo run --bin client`
### Server
`cargo run --bin server IP:PORT DB_PATH`, where `DB_PATH` is a path to the sqlite3 database with the served data.