pub struct ClientError<'a> {
    pub title: &'a str,
    pub text: &'a str,
    pub err: ErrorWrapper,
}

#[derive(Debug)]
pub enum ErrorWrapper {
    IoError(std::io::Error),
    AddrParseError(std::net::AddrParseError),
}
