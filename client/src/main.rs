mod error;
use error::{ClientError, ErrorWrapper};
use gtk::{gio, prelude::*};

fn main() {
    gio::resources_register_include!("dtest.gresource").expect("Failed to register resources!");
    let app = gtk::Application::builder()
        .application_id("ru.dlblv.dtest.client")
        .build();
    app.connect_activate(app_activate);
    app.run();
}

fn app_activate(app: &gtk::Application) {
    let builder = gtk::Builder::from_resource("/ru/dlblv/dtest/main.ui");
    let wnd_main: gtk::ApplicationWindow = builder.object("wnd_main").expect("wnd_main");
    app.add_window(&wnd_main);
    let btn_request: gtk::Button = builder.object("btn_request").expect("btn_request");
    let txt_log: gtk::TextView = builder.object("txt_log").expect("txt_log");
    let log_buf = txt_log.buffer();
    let etr_ip: gtk::Entry = builder.object("etr_ip").expect("etr_ip");
    let spn_port: gtk::SpinButton = builder.object("spn_port").expect("spn_port");
    let spn_timeout: gtk::SpinButton = builder.object("spn_timeout").expect("spn_timeout");
    btn_request.connect_clicked({
        let log_buf = log_buf.clone();
        let wnd_main = wnd_main.clone();
        move |_| {
            let timeout = std::time::Duration::from_millis(spn_timeout.value() as u64);
            match do_request(
                &log_buf,
                &etr_ip.text().as_str(),
                spn_port.value() as _,
                timeout,
            ) {
                Ok(resp) => {
                    log(
                        &log_buf,
                        "Request finished!\nReceived data:\n============================",
                    );
                    for s in resp {
                        log(&log_buf, &s);
                    }
                    log(&log_buf, "============================");
                }
                Err(err) => {
                    log(
                        &log_buf,
                        &format!("Error occured: {}\nDebug info: {:?}", err.text, err.err),
                    );
                    let msg = gtk::MessageDialog::builder()
                        .transient_for(&wnd_main)
                        .modal(true)
                        .message_type(gtk::MessageType::Error)
                        .buttons(gtk::ButtonsType::Ok)
                        .text(err.text)
                        .title(err.title)
                        .build();
                    msg.connect_response(|d, _| {
                        d.destroy();
                    });
                    msg.show();
                }
            }
        }
    });
    wnd_main.connect_show({
        let log_buf = log_buf.clone();
        move |_| {
            log(&log_buf, "Application started!");
        }
    });
    wnd_main.show();
}

fn log(log_buffer: &gtk::TextBuffer, msg: &str) {
    log_buffer.insert(&mut log_buffer.end_iter(), &format!("> {}\n", msg));
}

fn do_request<'a>(
    logger: &gtk::TextBuffer,
    address: &str,
    port: u16,
    timeout: std::time::Duration,
) -> Result<Vec<String>, ClientError<'a>> {
    let address = address
        .parse::<std::net::IpAddr>()
        .map_err(|err| ClientError {
            title: "Invalid address!",
            text: "Seems that you've entered invalid IP!\nPlease, check everything and try again!",
            err: ErrorWrapper::AddrParseError(err),
        })?;

    let sock = std::net::UdpSocket::bind("[::]:0").map_err(|err| ClientError {
        title: "Socket error!",
        text: "Unable to bind socket!",
        err: ErrorWrapper::IoError(err),
    })?;
    sock.set_read_timeout(Some(timeout))
        .map_err(|err| ClientError {
            title: "Socket error!",
            text: "Unable to set timeout!",
            err: ErrorWrapper::IoError(err),
        })?;

    log(logger, &format!("Connecting to {}...", address));
    sock.connect((address, port)).map_err(|err| ClientError {
        title: "Socket error!",
        text: "Unable to connect to the address given!",
        err: ErrorWrapper::IoError(err),
    })?;

    log(logger, &format!("Sending request to {}...", address));
    sock.send("request".as_bytes()).map_err(|err| ClientError {
        title: "Send failed!",
        text: "Failed to send data to the address given!",
        err: ErrorWrapper::IoError(err),
    })?;

    let mut res = Vec::new();
    let mut buf = [0u8; 1024];
    let mut length: usize;
    while {
        length = sock.recv(&mut buf).map_err(|err| ClientError {
            title: "Connection failed!",
            text: "Something went wrong while trying to recv information!",
            err: ErrorWrapper::IoError(err),
        })?;
        length
    } > 0
    {
        res.push(String::from_utf8(buf[0..length].to_vec()).unwrap());
    }
    Ok(res)
}
