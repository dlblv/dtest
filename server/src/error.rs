#[derive(Debug)]
pub enum ServerError {
    AddrParseError(std::net::AddrParseError),
    IoError(std::io::Error),
    DbError(rusqlite::Error),
}

impl From<std::net::AddrParseError> for ServerError {
    fn from(e: std::net::AddrParseError) -> Self {
        ServerError::AddrParseError(e)
    }
}

impl From<std::io::Error> for ServerError {
    fn from(e: std::io::Error) -> Self {
        ServerError::IoError(e)
    }
}

impl From<rusqlite::Error> for ServerError {
    fn from(e: rusqlite::Error) -> Self {
        ServerError::DbError(e)
    }
}
