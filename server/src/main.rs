mod error;
use error::ServerError;
use rusqlite::{params, Connection, Result};
fn main() -> Result<(), ServerError> {
    let (addr, db_path) = {
        let args = std::env::args().collect::<Vec<_>>();
        if args.len() != 3 {
            println!("Usage {} IP:PORT DB_PATH", args[0]);
            std::process::exit(1);
        }
        let addr = args[1].parse::<std::net::SocketAddr>()?;
        let db_path = args[2].clone();
        (addr, db_path)
    };
    let socket = std::net::UdpSocket::bind(addr)?;
    let mut buf = [0u8; 1024];
    let db = init_db(&db_path)?;
    loop {
        let (length, client) = socket.recv_from(&mut buf).expect("recv_from");
        if &buf[0..length] == "request".as_bytes() {
            println!("Serving request from {:?}", client);
            let mut stmt = db.prepare("SELECT * FROM table1")?;
            let mut rows = stmt.query([])?;
            while let Some(row) = rows.next()? {
                let id: usize = row.get(0)?;
                let data: String = row.get(1)?;
                socket.send_to((&format!("{} | {}", id, data)).as_bytes(), client)?;
            }
            socket.send_to(&[], client)?;
        }
    }
}

fn init_db(db_path: &str) -> Result<Connection> {
    let conn = Connection::open(db_path)?;
    conn.execute(
        "CREATE TABLE IF NOT EXISTS table1 (
            id      INTEGER PRIMARY KEY ASC AUTOINCREMENT NOT NULL DEFAULT 0,
            data    TEXT
        )",
        [],
    )?;
    if !conn.prepare("SELECT * FROM table1")?.exists([])? {
        println!("Database is empty, filling it with default values");
        let data = [(1, "Alpha"), (2, "Beta"), (3, "Gamma"), (4, "Delta")];
        for item in data {
            conn.execute(
                "INSERT INTO table1(id, data) VALUES(?1, ?2)",
                params![item.0, item.1],
            )?;
        }
    }
    Ok(conn)
}
